<?php

namespace App\Http\Controllers\Api;

use App\Model\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();
        return response()->json($subjects);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'class_id' => 'required',
            'subject_name' => 'required|unique:subjects|max:25',
            'subject_code' => 'required|unique:subjects|max:20',
        ]);

        $subjects = new Subject();
        $subjects->class_id = $request['class_id'] ;
        $subjects->subject_name = $request['subject_name'] ;
        $subjects->subject_code = $request['subject_code'] ;
        $subjects->save();
        return response()->json($subjects);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = Subject::where(['id'=>$id])->first();
        return response()->json($subject);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'class_id' => 'required',
            'subject_name' => 'required|unique:subjects|max:25',
            'subject_code' => 'required|unique:subjects|max:20',
        ]);

        $subjects =  Subject::find($id) ;
        $subjects->class_id = $request['class_id'] ;
        $subjects->subject_name = $request['subject_name'] ;
        $subjects->subject_code = $request['subject_code'] ;
        $subjects->save();
        return response()->json($subjects);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Subject::where(['id'=>$id])->delete();
        // return response()->json($subject);
        return response('ok') ;
    }
}
