<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Student;
use Illuminate\Support\Facades\Hash;    //For password
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return response()->json($students);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'class_id' => 'required',
            'section_id' => 'required',
            'name' => 'required|string|max:30',
            'phone' => 'required|regex:/(01)[0-9]{9}/',
            'email' => 'required|string|email|unique:students|max:25',
            'password' => 'required|string|max:20',
            'photo' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);


        $students  = new Student();
        $students ->class_id = $request['class_id'] ;
        $students ->section_id = $request['section_id'] ;
        $students ->name = $request['name'] ;
        $students ->phone = $request['phone'] ;
        $students ->email = $request['email'] ;
        $students ->password = Hash::make($request['password']);
        $students ->photo = $request['photo'] ;
        $students ->address = $request['address'] ;
        $students ->gender = $request['gender'] ;
        $students ->save();
        return response()->json($students);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::where(['id'=>$id])->first();
        return response()->json($student);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'class_id' => 'required',
            'section_id' => 'required',
            'name' => 'required|string|max:30',
            'phone' => 'required|regex:/(01)[0-9]{9}/',
            'email' => 'required|string|email|unique:students|max:25',
            'password' => 'required|string|max:20',
            'photo' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        $students  =  Student::find($id);
        $students ->class_id   = $request['class_id'] ;
        $students ->section_id = $request['section_id'] ;
        $students ->name       = $request['name'] ;
        $students ->phone      = $request['phone'] ;
        $students ->email      = $request['email'] ;
        $students ->password   = Hash::make($request['password']);
        $students ->photo      = $request['photo'] ;
        $students ->address    = $request['address'] ;
        $students ->gender     = $request['gender'] ;
        $students ->save();
        return response()->json($students);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::where(['id'=>$id])->delete();
        // return response()->json($subject);
        return response('successfully Deleted') ;
    }
}
