<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Category;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $class = DB::table('categories')->get();
//        return response()->json($class);

        return Category::all();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'class_name' => 'required|unique:categories|max:255',

        ]);
         // Category::create($request->all());
        $a = new Category();
        $a->class_name = $request['class_name'] ;
        $a->save();
          return response('ok') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = DB::table('categories')->where('id',$id)->first();
        return response()->json($show);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'class_name' => 'required|unique:categories|max:255',

        ]);
        $a=Category::where('id',$id)->first();
        $a->class_name = $request['class_name'] ;
        $a->save();
        return response()->json($a);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Category::where(['id'=>$id])->delete();
//        $a = Category::where('id','$id')->first();
//        $a->delete();
        return response('ok') ;
    }
}
